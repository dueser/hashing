class Kundenliste extends List<Kunde>{}
public class Hashing {

    private final Kundenliste[] kundenarray = new Kundenliste[1000];

    public void einfeuegen(Kunde kunde) {
        int z = kunde.getKundennummer() % 1000;
        if (kundenarray[z] == null) {
            Kundenliste kundenliste = new Kundenliste();
            kundenliste.append(kunde);
            kundenarray[z] = kundenliste;
        } else {
            kundenarray[z].append(kunde);
        }
    }

    public Kunde suchen(int kundennummer) {
        int z = kundennummer % 1000;
        if (kundenarray[z] == null) {
            return null;
        }
        kundenarray[z].toFirst();
        while (kundenarray[z].hasAccess()) {
            if (kundennummer == kundenarray[z].getContent().getKundennummer()) {
                return kundenarray[z].getContent();
            }
            kundenarray[z].next();
        }
        return null;
    }


    public static void main(String[] args) {
        var fred = new Kunde(1234, "Fred", 81.2);
        var ang = new Kunde(4736, "Angelika", 80.0);
        var magg = new Kunde(2234, "Magdalena", 50.0);

        var hashing = new Hashing();
        hashing.einfeuegen(fred);
        hashing.einfeuegen(ang);
        hashing.einfeuegen(magg);

        var kunde1 = hashing.suchen(2234);
        System.out.println(kunde1);

        var kunde2 = hashing.suchen(3235);
        System.out.println(kunde2);
    }

}
