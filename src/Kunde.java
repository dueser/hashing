public class Kunde {

    private int kundennummer;
    private String name;
    private double gewicht;

    public Kunde(int kundennummer, String name, double gewicht) {
        this.kundennummer = kundennummer;
        this.name = name;
        this.gewicht = gewicht;
    }

    public int getKundennummer() {
        return kundennummer;
    }

    public void setKundennummer(int kundennummer) {
        this.kundennummer = kundennummer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGewicht() {
        return gewicht;
    }

    public void setGewicht(double gewicht) {
        this.gewicht = gewicht;
    }

    @Override
    public String toString() {
        return "Kunde{" +
                "kundennummer=" + kundennummer +
                ", name='" + name + '\'' +
                ", gewicht=" + gewicht +
                '}';
    }
}
